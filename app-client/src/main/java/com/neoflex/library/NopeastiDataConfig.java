package com.neoflex.library;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

@ComponentScan
@EnableAutoConfiguration
@SpringBootConfiguration
@PropertySource(value = "client.properties")
public class NopeastiDataConfig {
    // no main needed here as this is library consumed by business layer
}