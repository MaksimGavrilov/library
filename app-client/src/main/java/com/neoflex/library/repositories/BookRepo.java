package com.neoflex.library.repositories;

import com.neoflex.library.models.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;


@Repository
public interface BookRepo extends JpaRepository<Book, Integer> {
    Book findBookByBookName(String bookName);
}
