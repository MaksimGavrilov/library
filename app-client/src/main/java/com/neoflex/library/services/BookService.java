package com.neoflex.library.services;

import com.neoflex.library.models.Book;

public interface BookService {

    Iterable<Book> findAllBooks();

    Book findById(Integer bookId);

    Book findByBookName(String bookName);

    Book updateBook(Integer bookId, Book book);

    Book insertBook(Book book);

    void deleteBook(Integer bookId);
}
