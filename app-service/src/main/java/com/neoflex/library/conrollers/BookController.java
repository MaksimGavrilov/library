package com.neoflex.library.conrollers;

import com.neoflex.library.models.Book;
import com.neoflex.library.servicesImpl.BookServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/books")
@RequiredArgsConstructor
public class BookController {

    private final BookServiceImpl bookService;

    @GetMapping("/all")
    public Iterable<Book> findAllBooks(){
        System.out.println(bookService.findAllBooks());
        return bookService.findAllBooks();
    }

    @GetMapping("/byId/{id}")
    public Book findBook(@PathVariable(name = "id") Integer bookId){
        return bookService.findById(bookId);
    }

    @GetMapping("/byName/{bookName}")
    public Book findBook(@PathVariable(name = "bookName") String bookName){
        return bookService.findByBookName(bookName);
    }

    @PutMapping("/update/{id}")
    public Book updateBook(@PathVariable(name = "id") Integer bookId, @RequestBody Book newBook){
        return bookService.updateBook(bookId,newBook);
    }

    @PostMapping("/create")
    public Book createBook(@RequestBody Book book){
        return bookService.insertBook(book);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteBook(@PathVariable(name = "id") Integer bookId){
        bookService.deleteBook(bookId);
    }
}

