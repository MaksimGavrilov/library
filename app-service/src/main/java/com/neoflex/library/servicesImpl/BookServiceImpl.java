package com.neoflex.library.servicesImpl;


import com.neoflex.library.models.Book;
import com.neoflex.library.repositories.BookRepo;
import com.neoflex.library.services.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {

    private final BookRepo bookRepo;


    @Override
    public List<Book> findAllBooks() {
        System.out.println(bookRepo.findAll());
        return bookRepo.findAll();
    }

    @Override
    public Book findById(Integer bookId) {
        return bookRepo.findById(bookId).orElse(null);
    }

    @Override
    public Book findByBookName(String bookName) {
        return bookRepo.findBookByBookName(bookName);
    }

    @Override
    public Book updateBook(Integer bookId, Book book) {

        book.setId(bookId);

        return bookRepo.save(book);
    }

    @Override
    public Book insertBook(Book book) {
        return bookRepo.save(book);
    }

    @Override
    public void deleteBook(Integer bookId) {
        bookRepo.deleteById(bookId);
    }
}
